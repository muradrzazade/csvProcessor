/***************************************************************************************
*    Title: Read and parse CSV file in Java/Advanced Solutions/CSVUtils.java
*    Author: Mkyoung.com
*    Date: July 18, 2016
*    Availability: https://www.mkyong.com/java/how-to-read-and-parse-csv-file-in-java/
*
***************************************************************************************/
package readInReverse;

import java.util.ArrayList;
import java.util.List;

/*
 * This class is responsible for reading csv rows splitted by "|" and parse them in to an array where each column have is fixed location in the array.
 * */

public class CSVRowParser {

    private static final char DEFAULT_SEPARATOR = '|'; //character by which columns separated	
    private static final char DEFAULT_QUOTE = '{';
       
    public static List<String> parseLine(String cvsLine) {
        return parseLine(cvsLine, DEFAULT_SEPARATOR, DEFAULT_QUOTE);
    }
 
    public static List<String> parseLine(String cvsLine, char separators) {
        return parseLine(cvsLine, separators, DEFAULT_QUOTE);
    }

    @SuppressWarnings("null")
	public static List<String> parseLine(String cvsLine, char separators, char customQuote) {

        List<String> result = new ArrayList<>();
        //if empty, return!
       
        if (cvsLine == null && cvsLine.isEmpty()) { return result; }
        if (customQuote == ' ') { customQuote = DEFAULT_QUOTE; }
        if (separators == ' ') { separators = DEFAULT_SEPARATOR; }

        StringBuffer curVal = new StringBuffer();

        boolean inQuotes = false;
        boolean startCollectChar = false;
        boolean doubleQuotesInColumn = false;
        char[] chars = cvsLine.toCharArray();

        for (char ch : chars) {
            if(inQuotes){
                startCollectChar = true;
                if (ch == customQuote) {
                    inQuotes = false;
                    doubleQuotesInColumn = false;
                } else {
                    //Fixed : allow "" in custom quote enclosed
                    if (ch == '\"') {
                        if (!doubleQuotesInColumn) {
                            curVal.append(ch);
                            doubleQuotesInColumn = true; }
                    } else { curVal.append(ch); }
                }
            } else { if (ch == customQuote) {
                    inQuotes = true;
                    //Fixed : allow "" in empty quote enclosed
                    if (chars[0] != '"' && customQuote == '\"') { curVal.append('"'); }
                    //double quotes in column will hit this!
                    if (startCollectChar) { curVal.append('"'); }
                } else if (ch == separators) {
                    result.add(curVal.toString());
                    curVal = new StringBuffer();
                    startCollectChar = false;
                } else if (ch == '\r') { continue; //ignore LF characters                
                } else if (ch == '\n') { break;    //the end, break!
                } else { curVal.append(ch); }
            }
        }
        result.add(curVal.toString());       
        return result;
    }		
}
