/********************************************************
 * 	Created by Murad Rzazade on 12/03/2018
 * 	Copyright © 2018. All rights reserved.
 * 	Last modified 24/04/2018
 * 	Unauthorized copying or using of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Murad Rzazade <muradrzazade@mail.ru>, April 2018
 * 	
 * @author Murad Rzazade
 * 
 ******************************************************/

package readInReverse;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


//The class is responsible for collecting and storing data of stock movement file in treemap which stores data in sorted manner by an artcile code.
public class CSVSorter  {
	
	private static Map<String, List<String>> map;
	private static double processedPercentage; 
	private static int processedRow = 0;
	
	public static void sort(String inputFile) throws Exception {
		
	    BufferedReader reader = new BufferedReader(new FileReader(inputFile));
        setMap(new TreeMap<String, List<String>>());
        processedRow = 0;
        processedPercentage = CSVCollector.progress;
        
        String line;
        //= reader.readLine();//read header (skip the header)
        while ((line = reader.readLine()) != null) {
        	
        	updateProgressBar();
            String key = getArticleCode(line);
            List<String> listOfLines = getMap().get(key);
            if (listOfLines == null) {
            	listOfLines = new LinkedList<String>();
                getMap().put(key, listOfLines);
            }
            listOfLines.add(line);
        }      
       reader.close();
    }

    private static String getArticleCode(String line) {
        return line.split("\\|")[1];// separator and column which to sort.
    }

	public static Map<String, List<String>> getMap() {
		return map;
	}

	private static void setMap(Map<String, List<String>> map) {
		CSVSorter.map = map;
	}
	
	private static void updateProgressBar() {
		processedRow++;
		double progress = (double)processedRow*CSVProcessor.SINGLE_ROW_SIZE*1024/CSVCollector.fileSizeInBytes*CSVCollector.fileCount;
		double proportion = 100/CSVCollector.fileCount;
		CSVCollector.progress= (double)(proportion*0.25*progress+processedPercentage);
		CSVCollector.progressBar.setValue((int)(CSVCollector.progress));  
	}
}
