/********************************************************
 * 	Created by Murad Rzazade on 12/03/2018
 * 	Copyright © 2018. All rights reserved.
 * 	Last modified 30/04/2020
 * 	Unauthorized copying or using of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Murad Rzazade <muradrzazade@mail.ru>, April 2018
 * 	
 * @author Murad Rzazade
 * 
 ******************************************************/
package readInReverse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.border.TitledBorder;

public class CSVProcessor {

    private static List<Double> costTransactions,quantityTransactions,negativeCostTransactions,positiveCostTransactions,
    	negativeQuantityTransactions,positiveQunatityTransactipons,negativeCostTransactionsOnWAC;
    private static List<String> line;
    private static double PPC,WAC,processedPercentage,currentQuantityStock = 0; 
    private static boolean addInitialBalance = true, transactionOccuredOnNonConsginmentAritcleOnCurrentSUCode;
    private static String SUCode = "";
    private static HashMap<String,ArrayList<Double>> movementsByProduct, movementsByProductOnWAC;
    public static int totalRowCount = 0, idCount = 0, processedRow = 0;
	public static final double SINGLE_ROW_SIZE = 95082.4013671875/326021;
    public static final String[] MOVEMENT_TYPES = {"Sales","Production","Reception","Consump","Correction","Inventory","Clrce Sale","IN transfe",
    		"Kit const","Kit split", "OUT trans","Transf IN","Transf OUT","VRN modif","Sup ret ad","Supp retur","ExpD sales","InsPromSal","Unblocking","Shipment",
    		"Blockage","PCut Sales","Ship adjus","LF. sale","Cust retur"};
    
    public static double sum;
    
    public static void processSite(String fileName) throws Exception {

    	//WHEN PROCESSING DC CHANGE i to 3 (number may vary depending on number of supply units)
    	for(int i = 0; i < 4; i++) {
    		SUCode = String.valueOf(i);
    		processID(fileName);
    	}
    }
    
    public static void processID(String fileName) throws Exception {
    	
    	processedRow = 0; processedPercentage = CSVCollector.progress;
        ((TitledBorder) CSVCollector.border).setTitle("Processing..."+fileName);    
    	String currentInvetoryName = null, currentSite = null, 
    			currentMovementType,currentOperationalCode,currentArticleCode,currentDate;
    	transactionOccuredOnNonConsginmentAritcleOnCurrentSUCode = false;
     
    	for(String id: CSVSorter.getMap().keySet()){    		
    		
    		resetValues();
    		    		
 innerLoop: for (String transactionLine: CSVSorter.getMap().get(id)) {
    	 
    			totalRowCount++; processedRow++;    	      	
    			line = CSVRowParser.parseLine(transactionLine);
    			
    			currentSite = line.get(0);
    			currentArticleCode = line.get(1);
    			currentInvetoryName = line.get(2); 
    			currentDate = line.get(7);
    			currentMovementType = line.get(9);
    			currentOperationalCode = line.get(19);
    			
    		
    			//skip the consignment products and non-current SU code.
    			//WHEN PROCESSING DC CHANGE line.get(4) to line.get(3) as DC have to be calculated based on LV code not SU code.   
    			if(!line.get(4).equals(SUCode)) {
    				continue innerLoop;
    			
    			} else if (SystemConsignmentProducts.isConsignmentProduct(currentArticleCode)
    					&& SystemConsignmentProducts.isWithinConsignmentDates(currentSite, currentArticleCode, currentDate) ) {    	 
    				continue innerLoop;
    			
    			}else {
    				transactionOccuredOnNonConsginmentAritcleOnCurrentSUCode = true;
    			}
    			
       
    			if(addInitialBalance) {
    				costTransactions.add(SystemBalances.getOpeningCost(currentArticleCode,SUCode,currentSite));
    				quantityTransactions.add(SystemBalances.getOpeningQuantity(currentArticleCode,SUCode,currentSite));
    				WAC = Double.parseDouble(line.get(45));
    				addInitialBalance = false;		
    				currentQuantityStock = quantityTransactions.get(0);
    			}
	  		
    			boolean takeValue = true;
    			double currentCostTransaction = Double.parseDouble(line.get(26).replaceAll(",", ""));
    			double currentQuantityTransaction = Double.parseDouble(line.get(14).replaceAll(",", ""));
      			
    			if(currentMovementType.equals("Sales") && currentCostTransaction > 0 ) {
    				for(int i = 0; i < line.size(); i++) {
    					CSVCollector.outCons.print(line.get(i)+"|");
    				}
    				CSVCollector.outCons.println();
    			}
    			
    			if(currentMovementType.equals("Sales") && currentCostTransaction > 0)
    				sum+=currentCostTransaction;
    			
    			costTransactions.add(currentCostTransaction);
    			quantityTransactions.add(currentQuantityTransaction);
    			currentQuantityStock += currentQuantityTransaction;
    			
    			//Map which will store list of transactions taken from the system by movement type
    			ArrayList<Double> listOfLinesByProduct =  movementsByProduct.get(currentMovementType);
    			if (listOfLinesByProduct == null) {
    				listOfLinesByProduct = new ArrayList<Double>();
    				movementsByProduct.put(currentMovementType, listOfLinesByProduct);
    			}
    			//as the list is the reference to the map when list updates it will put values automatically in to the map.
    			listOfLinesByProduct.add(currentCostTransaction);
    			
    			//Map which will store list of transactions by movement types with an applied recalculated WAC
    			ArrayList<Double> listOfLinesByProductOnWAC =  movementsByProductOnWAC.get(currentMovementType);
    			if (listOfLinesByProductOnWAC == null) {
    				listOfLinesByProductOnWAC = new ArrayList<Double>();
    				movementsByProductOnWAC.put(currentMovementType, listOfLinesByProductOnWAC);	
    			}

    			if(currentQuantityTransaction < 0) {
    				
    				negativeQuantityTransactions.add(currentQuantityTransaction);
    			 				
    				//check if this product is a retails own Manufactured article which is made from the ingredients
    				if(SystemIngredients.getReceipts().contains(currentArticleCode)) {
    					if(currentMovementType.equals("Sales") && !currentOperationalCode.equals("0")
    							&& currentCostTransaction < 0){
    						//Get the cost of ingredients applied to the product.
    						if(SystemIngredients.getConsumptionByOperationalCode().get(currentOperationalCode)!=null) { 
	
    								negativeCostTransactionsOnWAC.add(getSumOf(SystemIngredients.
    										getConsumptionByOperationalCode().get(currentOperationalCode)));
    								listOfLinesByProductOnWAC.add(getSumOf(SystemIngredients.
    										getConsumptionByOperationalCode().get(currentOperationalCode)));
    								takeValue = false;
    								
    						}else {					
    							negativeCostTransactionsOnWAC.add(currentCostTransaction);
    							listOfLinesByProductOnWAC.add(currentCostTransaction);
    							takeValue = false;
   							}//case when no consumption with this operational code happened	
    					}//sales with existing operational code 
    					
    					if(currentMovementType.equals("Sales") && currentOperationalCode.equals("0") && currentCostTransaction < 0
    							||currentMovementType.equals("Correction") || currentMovementType.equals("Consump")) {
    						negativeCostTransactionsOnWAC.add(currentCostTransaction);
    						listOfLinesByProductOnWAC.add(currentCostTransaction);
    						takeValue = false;    						
    					}//sales with no operational code (code=zero) 
        			}
    				
    				if(!currentMovementType.equals("VRN modif") && takeValue) {		 
    					negativeCostTransactionsOnWAC.add(WAC*currentQuantityTransaction);
    					listOfLinesByProductOnWAC.add(currentQuantityTransaction*WAC);
    					takeValue=true;
    				 }//apply WAC to all movement type transactions except the VRN Modification
    				 				
    			}else
    				positiveQunatityTransactipons.add(currentQuantityTransaction);
    			
    			if(currentCostTransaction > 0) {
    				 
    				PPC = costTransactions.get(costTransactions.size()-1) / 
    						quantityTransactions.get(quantityTransactions.size()-1);
    				
    				//When positive Sales, Correction or Inventory happens apply WAC for it 
    				if(currentMovementType.equals("Sales")||currentMovementType.equals("Correction")
    						||currentMovementType.equals("LF. sale")||currentMovementType.equals("Inventory")) {
    					
    						positiveCostTransactions.add(currentQuantityTransaction*WAC);
    						listOfLinesByProductOnWAC.add(currentQuantityTransaction*WAC);
    					
    				}else {
    					positiveCostTransactions.add(currentCostTransaction);
    					listOfLinesByProductOnWAC.add(currentCostTransaction);
    				}
    				
    				//Calculate WAC if there is product in the stock and for the following movement types only (if there is no stock theoretical WAC is taken)
    				if((currentMovementType.equals("Reception")||currentMovementType.equals("Transf IN")
    						||currentMovementType.equals("IN transfe")||currentMovementType.equals("VRN modif")
    						||currentMovementType.equals("Blockage")||currentMovementType.equals("Unblocking")) 
    						&& currentQuantityStock > 0 && getSumOf(costTransactions) / getSumOf(quantityTransactions) > 0 
    						&& Double.isFinite(getSumOf(costTransactions) / getSumOf(quantityTransactions)))  {				
    					 WAC = getSumOf(costTransactions) / getSumOf(quantityTransactions); 
    				}
    			}else{ 				
    				negativeCostTransactions.add(currentCostTransaction);
    				if(currentMovementType.equals("VRN modif")) {
    					negativeCostTransactionsOnWAC.add(currentCostTransaction);
    					listOfLinesByProductOnWAC.add(currentCostTransaction);
    					
    					if(getSumOf(costTransactions) / getSumOf(quantityTransactions) > 0 
    							&& Double.isFinite(getSumOf(costTransactions) / getSumOf(quantityTransactions))) 
    						WAC = getSumOf(costTransactions) / getSumOf(quantityTransactions);			
    				}//for VRN Modification movements don't apply WAC
    			}
    			
    			//Check if there is product in the stock. If not WAC is taken from the theoretical WAC list.
    			if(currentQuantityStock <= 0.0000001) { 				
    				WAC = SystemTheoreticalCosts.
							  getTheoreticalCost(currentSite, currentArticleCode, currentDate, SUCode) == 0 ? WAC 
							: SystemTheoreticalCosts.
									  getTheoreticalCost(currentSite, currentArticleCode, currentDate, SUCode);
    				WAC = Double.parseDouble(String.valueOf(WAC)
   						 .replaceAll("E", "").replaceAll("e", "").replaceAll("-", ""));	
    			}		
    			updateProgressBar();
 			}
    		
    	if(transactionOccuredOnNonConsginmentAritcleOnCurrentSUCode) {
    		print(currentInvetoryName, id,currentSite);
    		idCount++;
    	}        	   
        	   
        addInitialBalance = true;         
    	}//for
    }//processID
     
    private static void print(String currentInvetoryName, String articleCode, String site) {
    	
        CSVCollector.out.format("%s%s%s%s%s%s%s%s%s%s%s%s%s%s",
        		site+"|", articleCode+"|",SUCode+"|",costTransactions.get(0)+"|",quantityTransactions.get(0)+"|",
        		getSumOf(negativeCostTransactions)+"|",getSumOf(negativeCostTransactionsOnWAC)+"|",getSumOf(positiveCostTransactions)+"|",
        		getSumOf(negativeQuantityTransactions)+"|",getSumOf(positiveQunatityTransactipons)+"|",
        		getSumOf(costTransactions)+"|" ,getSumOf(quantityTransactions)+"|",PPC+"|",WAC+"|");
	
    	for(String movementType: MOVEMENT_TYPES) 
    		CSVCollector.out.format("%s%s",getSumOf(movementsByProduct.get(movementType))+"|",getSumOf(movementsByProductOnWAC.get(movementType))+"|");
    	
    	CSVCollector.out.format("%s%s%s%s%s%n", 
    			 SystemBalances.getOpeningCost(articleCode,SUCode,site)+"|",
    			 SystemBalances.getOpeningQuantity(articleCode,SUCode,site)+"|",
    			 SystemBalances.getClosingCost(articleCode,SUCode,site)+"|", 
    			 SystemBalances.getClosingQuantity(articleCode,SUCode,site)+"|","   "+currentInvetoryName); 	
    }
    
    private static void resetValues() {
    	
    	quantityTransactions = new ArrayList<Double>();
        costTransactions = new ArrayList<Double>();
        negativeCostTransactions = new ArrayList<Double>();
        positiveCostTransactions = new ArrayList<Double>();
        negativeQuantityTransactions =  new ArrayList<Double>();
        positiveQunatityTransactipons = new ArrayList<Double>();
        negativeCostTransactionsOnWAC = new ArrayList<Double>();
		movementsByProduct = new HashMap<String, ArrayList<Double>>();
		movementsByProductOnWAC = new HashMap<String, ArrayList<Double>>();
		transactionOccuredOnNonConsginmentAritcleOnCurrentSUCode = false ; 
	   	currentQuantityStock = 0;
		WAC = 0; 
    	PPC = 0; 
    }
    
    private static void updateProgressBar() {

		double progress = (double)processedRow*SINGLE_ROW_SIZE*1024/CSVCollector.fileSizeInBytes*CSVCollector.fileCount/2.886;
		double proportion = 100/CSVCollector.fileCount;

		CSVCollector.progress= (double)(proportion*0.76*progress+processedPercentage);
		CSVCollector.progressBar.setValue((int)(CSVCollector.progress));    
    }
        
    private static double getSumOf(List<Double> list) {
      try {
    	double total = 0;
        for (Double transaction : list) {
            total += transaction == null ? 0.0 : transaction;
        }
        return total;
        }catch(Exception e) {
        	return 0;
        }    
    }
}
