/********************************************************
 * 	Created by Murad Rzazade on 12/03/2018
 * 	Copyright © 2018. All rights reserved.
 * 	Last modified 24/04/2018
 * 	Unauthorized copying or using of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Murad Rzazade <muradrzazade@mail.ru>, April 2018
 * 	
 * @author Murad Rzazade
 * 
 ******************************************************/

package readInReverse;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class SystemIngredients {
	
	private static Set<String> ingredients = new LinkedHashSet<String>();
	private static Set<String> receipts = new LinkedHashSet<String>();
	private static Map<String, ArrayList<Double>> ConsumptionByOperationalCode;
	private static List<String> line;
	private static boolean addInitialBalance = true;
	private static double WAC,currentQuantityStock = 0;
	private static List<Double> costTransactions,quantityTransactionList,negativeCostTransactionsOnWAC;
	 
	public static void calculateIngredients() throws Exception {
		
        setConsumptionByOperationalCode(new TreeMap<String, ArrayList<Double>>());
        String currentSite,currentMovementType,currentOperationalCode,currentArticleCode,currentDate;	
		    	
		for(int SUCode = 0 ; SUCode<4; SUCode++) {	
			
ingredients: for(String id : ingredients) {	
	
	    		if(CSVSorter.getMap().get(id)==null) 
	    			continue ingredients;
	    		
	    		resetValues();   
	    		
idTransactions: for (String transactionLine: CSVSorter.getMap().get(id)) { 
	    				line = CSVRowParser.parseLine(transactionLine);
	    				
	    				//as the loop re-reads movement stock several time based on SU Code, skip transactions of non-current SU.
	    				if(!line.get(4).equals(String.valueOf(SUCode))) 
	    					continue idTransactions;
	    				
	    				currentSite = line.get(0);  
	        			currentArticleCode = line.get(1);
	        			currentDate = line.get(7);
	        			currentMovementType = line.get(9);
	        			currentOperationalCode = line.get(19);
	    				
	    				if(addInitialBalance) {
	    					costTransactions.add(SystemBalances.getOpeningCost(currentArticleCode,String.valueOf(SUCode),currentSite));
	    					quantityTransactionList.add(SystemBalances.getOpeningQuantity(currentArticleCode,String.valueOf(SUCode),currentSite));
	    					WAC = Double.parseDouble(line.get(45));
	    					addInitialBalance = false;	
	     					currentQuantityStock=quantityTransactionList.get(0);
	    				}
	    				
	    				boolean takeValue = true;
	    				double currentCostTransaction = Double.parseDouble(line.get(26).replaceAll(",", ""));
	    				double currentQuantityTransaction = Double.parseDouble(line.get(14).replaceAll(",", ""));
	    				costTransactions.add(currentCostTransaction);
	    				quantityTransactionList.add(currentQuantityTransaction);
	    				currentQuantityStock += currentQuantityTransaction;
	     			
	    				//Check if there is product in the stock. If not WAC is taken from the theoretical WAC list.
	    				if(currentQuantityStock <= 0) {
	    					WAC = SystemTheoreticalCosts.
	  							  getTheoreticalCost(currentSite, currentArticleCode, currentDate, String.valueOf(SUCode)) == 0 ? WAC 
	  							: SystemTheoreticalCosts.
	  									  getTheoreticalCost(currentSite, currentArticleCode, currentDate, String.valueOf(SUCode)); 
	    					WAC = Double.parseDouble(String.valueOf(WAC)
							 .replaceAll("E", "").replaceAll("e", "").replaceAll("-", ""));
	    				}
	    				
	    				if(currentQuantityTransaction < 0) {
	    					//check if this product is a retails own Manufactured article which is made from the ingredients
	    					//AS THIS CLASS IS RESPONSIBLE FOR INGREDIENTS ONLY THIS CASE IS POSSIBLE ONLY FOR SEMI-FINISHED GOODS
	    					//THE PRODUCTS WHICH ARE MANUFACTURED FROM INGREDIENTS BUT AT THE SAME TIME THEY ARE INGREDIENT OF THE UPPER PRODUCT.
	    					//IMPORTANT NOTE HERE IS THAT IN RECEIPT LIST SEMI PRODUCTS MUST COME AT THE END
	    					//BECAUSE PROGRAM HAS TO CALCULATE CONSUMPTION OF THEIR INGREDIENTS BEFORE COMING TO SEMI-PRODUCTS.
	    					if(getReceipts().contains(currentArticleCode)) {
	        					if(currentMovementType.equals("Sales") && !currentOperationalCode.equals("0") 
	        							&& currentCostTransaction < 0){
	        						//Get the cost of ingredients applied to the product.
	        						if(getConsumptionByOperationalCode().get(currentOperationalCode)!=null) {	        						
	        							negativeCostTransactionsOnWAC.add(getSumOf(getConsumptionByOperationalCode().get(currentOperationalCode)));
	        							takeValue=false;					
	        						}else {
	        							negativeCostTransactionsOnWAC.add(currentCostTransaction);
	        							takeValue=false;
	        						}//case when no consumption with this operational code happened
	        					}//sales with existing operational code 
	        					
	        					if(currentMovementType.equals("Sales") && currentOperationalCode.equals("0") && currentCostTransaction < 0
	        							||currentMovementType.equals("Correction") || currentMovementType.equals("Consump")) {				
	        						negativeCostTransactionsOnWAC.add(currentCostTransaction);
	        						takeValue = false;
	        					}//sales with no operational code (code=zero)
	            			}
	        				
	        				if(!currentMovementType.equals("VRN modif") && takeValue) {   				
	        					negativeCostTransactionsOnWAC.add(WAC*currentQuantityTransaction);
	        					takeValue=true;
	        				 }//apply WAC to all movement type transactions except the VRN Modification
	        				 if(currentMovementType.equals("VRN modif"))
	        					negativeCostTransactionsOnWAC.add(currentCostTransaction);
	     			
	    				}
	     			
	    				//For ingredients WAC is calculated for all movement types and if there is product in the stock(if there is no stock theoretical WAC is taken)
	    				if(currentCostTransaction >0 && currentQuantityStock>0) 
	    					WAC  = getSumOf(costTransactions) / getSumOf(quantityTransactionList);
	     				
	    				//If consumption made for an ingredient add calculated consumption value to the list referred of the corresponding operational code
	    				if(currentMovementType.equals("Consump") && currentCostTransaction <0) {
	    					if(!negativeCostTransactionsOnWAC.isEmpty()) {
	    						ArrayList<Double> listOfLines = getConsumptionByOperationalCode().get(currentOperationalCode);
	    			    		if (listOfLines == null) {
	    			    			listOfLines = new ArrayList<Double>();
	    			    			getConsumptionByOperationalCode().put(currentOperationalCode, listOfLines);
	    			    		}
	    			    		//add calculated consumption to list referenced to the operational code.
	    			    		listOfLines.add(negativeCostTransactionsOnWAC.get(negativeCostTransactionsOnWAC.size()-1));
	    					}
	    				}
	    			}
	    			addInitialBalance = true;   
	    		}
			}
		}
	
	public static void getIngredients(String path) throws IOException {
		
    	BufferedReader reader = new BufferedReader(new FileReader(path));
    		    	
    	String row = reader.readLine();//read header (skip the header)
    	while ((row = reader.readLine()) != null) {
    		String ingredient = getIngredient(row);
    		String receipt = getReceipts(row);
    		ingredients.add(ingredient);
    		getReceipts().add(receipt);
    		
    	}
    	reader.close(); 			
	}
	
	private static String getReceipts(String line) {
		return line.split(",")[0];
	}  
	
	private static String getIngredient(String line) {
		return line.split("\\|")[1];
	}  
	
	private static double getSumOf(List<Double> list) {
		try {
			double total = 0;
			for (Double transaction : list) {
				total += transaction == null ? 0.0 : transaction;
			}
			return total;
		}catch(Exception e) {
			return 0;
      }   
	}
	
	private static void resetValues() {
		
		quantityTransactionList = new ArrayList<Double>();
        costTransactions = new ArrayList<Double>();
        negativeCostTransactionsOnWAC = new ArrayList<Double>();
        currentQuantityStock = 0;
        WAC = 0;
	}

	/**
	 * @return the receipts
	 */
	public static Set<String> getReceipts() {
		return receipts;
	}

	/**
	 * @param receipts the receipts to set
	 */
	public static void setReceipts(Set<String> receipts) {
		SystemIngredients.receipts = receipts;
	}

	/**
	 * @return the consumptionByOperationalCode
	 */
	public static Map<String, ArrayList<Double>> getConsumptionByOperationalCode() {
		return ConsumptionByOperationalCode;
	}

	/**
	 * @param consumptionByOperationalCode the consumptionByOperationalCode to set
	 */
	public static void setConsumptionByOperationalCode(Map<String, ArrayList<Double>> consumptionByOperationalCode) {
		ConsumptionByOperationalCode = consumptionByOperationalCode;
	}
}
