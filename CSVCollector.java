/********************************************************
 * 	Created by Murad Rzazade on 12/03/2018
 * 	Copyright © 2018. All rights reserved.
 * 	Last modified 24/04/2018
 * 	Unauthorized copying or using of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Murad Rzazade <muradrzazade@mail.ru>, April 2018
 * 	
 * @author Murad Rzazade
 * 
 ******************************************************/

package readInReverse;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.WindowEvent;
import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileSystemView;

public class CSVCollector {


	public static PrintWriter out,outCons;
	public static String path = System.getProperty("user.home") + "/Desktop/";
	public static Border border;
	public static double progress = 0, fileCount = 0, fileSizeInBytes = 0;
	public static JProgressBar progressBar;
	private static JFileChooser chooseFiles;
	private static long createdMillis = 0; 
	
	public static void main(String[] args) throws Exception {
			
		 //Interface to choose required documents
		 chooseFiles = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		 chooseFiles.setMultiSelectionEnabled(true);
		
		 //FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV files (*csv)", "csv");
		//chooseFiles.setFileFilter(filter);
		 
		 int returnValue = chooseFiles.showOpenDialog(null);				
		 out = new PrintWriter(new FileWriter(path+"out.txt"));
		 outCons = new PrintWriter(new FileWriter(path+"outCons.txt"));

		 //ProgressBar to see progress on the user interface
		 JFrame progressFrame = new JFrame("CSV Processor");
		 progressFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 Container content = progressFrame.getContentPane();
		 progressBar = new JProgressBar();
		 progressBar.setValue(0);
		 progressBar.setStringPainted(true);
		 border = BorderFactory.createTitledBorder("Reading and Sorting...");
		 progressBar.setBorder(border);
		 content.add(progressBar, BorderLayout.NORTH);
		 progressFrame.setSize(1000, 105);
		 progressFrame.setVisible(true);
		 
		 //Collect and process the required data.
		 SystemBalances.getBalances(System.getProperty("user.home") + "/Desktop/bravo_Local/csv/balance.csv");
		 SystemConsignmentProducts.getConsignmentProducts(System.getProperty("user.home") + "/Desktop/bravo_Local/csv/Consignment_articles.csv");
		 SystemTheoreticalCosts.getTheoreticalCosts(System.getProperty("user.home") + "/Desktop/bravo_Local/csv/Theo_cost.csv");
		 SystemIngredients.getIngredients(System.getProperty("user.home") + "/Desktop/bravo_Local/csv/Recipe_ingredients.csv");
		 
		 if (returnValue == JFileChooser.APPROVE_OPTION) {
		    	
		   	createdMillis = System.currentTimeMillis();
			File[] selectedFile = chooseFiles.getSelectedFiles();
				
			//calculate total amount of files and their size which is required to calculate the progress 
			for(int i = 0; i<selectedFile.length;i++) {
				String path = selectedFile[i].getAbsolutePath();	
				if(path.contains(".csv")) {	
					fileCount++;
					fileSizeInBytes+=selectedFile[i].length(); 
				}
			}
			 CSVCollector.out.format("%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s"
			 		+ "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s"
			 		+ "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%n",
					 "Site|","ID|","SU|","Initial B|","Initial Q|","Outgoing B|", "Out on WAC|","Receipts B|","Outgoing Q|", "Receipt Q|",
					 "Ending B|","Ending Q|","PPC|","WAC|",
					 "Sales|","C Sales|","Production|","C Production|","Reception|","C Reception|","Consump|", "C Consump|","Correction|","C Correction|",
					 "Inventory|","C Inventory|","Clrce Sale|","C Clrce Sale|","IN transfe|","C IN transfe|","Kit const|","C Kit const|", "Kit split|", "C Kit split|",
					 "OUT trans|","C OUT trans|","Transf IN|","C Transf IN|","Transf OUT|","C Transf OUT|","VRN modif|","C VRN modif|",
					 "Sup ret ad|","C Sup ret ad|","Supp retur|","C Supp retur|","ExpD sales|","C ExpD sales|","InsPromSal|","C InsPromSal|",
					 "Unblocking|","C Unblocking|","Shipment|","C Shipment|","Blockage|","C Blockage|","PCut Sales|","C PCut Sales|","Ship adjus|","C Ship adjus|",
					 "LF. sale|","C LF. sale|","Cust retur|","C Cust retur|",
					 "System IB|","System IQ|","System EB|","System EQ|","Product Name");
			
			//sort and process selected files
			for(int i = 0; i < selectedFile.length; i++) {
				
				String path = selectedFile[i].getAbsolutePath();	
				String name = selectedFile[i].getName();
				((TitledBorder) border).setTitle("Sorting..."+name);
				CSVSorter.sort(path);	
				SystemIngredients.calculateIngredients();
				CSVProcessor.processSite(name);			
			}		
		}
		    
			long nowMillis = System.currentTimeMillis();
			out.println();
			out.println("\n"+"-------------------Total Rows:     "+CSVProcessor.totalRowCount/2);
			out.println("-------------------Total Products: "+(CSVProcessor.idCount));
			out.println("-------------------Total Seconds:  "+((double)(nowMillis - createdMillis) / 1000));
			System.out.println("-------------------Total Seconds:  "+((double)(nowMillis - createdMillis) / 1000));
			
			System.out.println("Supp return: "+CSVProcessor.sum);
			out.close();
		
			JOptionPane.showMessageDialog(null, "Please see the result in:  "+path+"out.txt", "CSV Processor", JOptionPane.INFORMATION_MESSAGE);
			progressFrame.dispatchEvent(new WindowEvent(progressFrame, WindowEvent.WINDOW_CLOSING));

	}
}
