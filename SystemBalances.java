/********************************************************
 * 	Created by Murad Rzazade on 12/043/2018
 * 	Copyright © 2018. All rights reserved.
 * 	Last modified 24/04/2018
 * 	Unauthorized copying or using of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Murad Rzazade <muradrzazade@mail.ru>, April 2018
 * 	
 * @author Murad Rzazade
 * 
 ******************************************************/

package readInReverse;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


//this class is responsible for collecting system opening and closing balances which then will be reconciled
public class SystemBalances {
	 
	private static Map<String, List<String>> map;
	private static Set<String> articleWithNoBalance= new HashSet<String>();
	
    public static double getOpeningQuantity(String articleCode, String SUCode, String site) {    
    	try {
    		List<String> listOfLines = map.get(articleCode);
    		double balance = 0;
    		
    		for(String thisLine : listOfLines) {   			
    			if(site.equals(getSiteCode(thisLine))) {
    				if(SUCode.equals(getSUCode(thisLine))) {
    					balance = Double.parseDouble(getOpeningQuantity(thisLine).replaceAll(",", ""));
    				}
    			}
    		}
    		return balance;
    	}catch(Exception e) {
    		articleWithNoBalance.add(articleCode);
    		return 0;	
    	}
    }
	
    public static double getOpeningCost(String articleCode, String SUCode, String site) {   	
    	try {
    		List<String> listOfLines = map.get(articleCode);
    		double balance = 0;
    		
    		for(String thisLine : listOfLines) {   			
    			if(site.equals(getSiteCode(thisLine))) {
    				if(SUCode.equals(getSUCode(thisLine))) {
    					balance = Double.parseDouble(getOpeningCost(thisLine).replaceAll(",", ""));
    				}
    			}
    		}    		
    		return balance;
    	}catch(Exception e) {
    		articleWithNoBalance.add(articleCode);
    		return 0;
    	}
    }

    public static double getClosingQuantity(String articleCode, String SUCode, String site) {
    	try {
    		List<String> listOfLines = map.get(articleCode);
    		double balance = 0;
    		
    		for(String thisLine : listOfLines) {    			
    			if(site.equals(getSiteCode(thisLine))) {
    				if(SUCode.equals(getSUCode(thisLine))) {
    					balance = Double.parseDouble(getClosingQuantity(thisLine).replaceAll(",", ""));
    				}
    			}
    		}
    		return balance;
    	}catch(Exception e) {
    		articleWithNoBalance.add(articleCode);
    		return 0;
    	}
    }
    
    public static double getClosingCost(String articleCode, String SUCode, String site) {
    	try {
    		List<String> listOfLines = map.get(articleCode);
    		double balance = 0;
    		
    		for(String thisLine : listOfLines) {    			
    			if(site.equals(getSiteCode(thisLine))) {
    				if(SUCode.equals(getSUCode(thisLine))) {
    					balance = Double.parseDouble(getClosingCost(thisLine).replaceAll(",", ""));
    				}
    			}
    		}
    		return balance;  		
    	}catch(Exception e) {
    		articleWithNoBalance.add(articleCode);
    		return 0;
    	}
    }
    
    public static void getBalances(String path) throws IOException {
		
    	BufferedReader reader = new BufferedReader(new FileReader(path));
    	map = new TreeMap<String, List<String>>();
    	
    	String line = reader.readLine();//read header (skip the header)
    	while ((line = reader.readLine()) != null) {
    		String key = getArticleCode(line);
    		List<String> listOfLines = map.get(key);
    		if (listOfLines == null) {
    			listOfLines = new LinkedList<String>();
    			map.put(key, listOfLines);
    		}
    		listOfLines.add(line);
    	}
    	reader.close();
    }
    
    private static String getSiteCode(String line) {
    	return line.split("\\|")[0];// separator and column in which to sort.
    }   
    
    private static String getArticleCode(String line) {
    	return line.split("\\|")[1];// separator and column in which to sort.
    }  
    
    private static String getSUCode(String line) {
    	return line.split("\\|")[2];// separator and column in which to sort.
    } 
    
    private static String getOpeningQuantity(String line) {
    	return line.split("\\|")[4];// separator and column in which to sort.
    } 
    
    private static String getOpeningCost(String line) {
    	return line.split("\\|")[5];// separator and column in which to sort.
    } 
    
    private static String getClosingQuantity(String line) {
    	return line.split("\\|")[6];// separator and column in which to sort.
    } 
    
    private static String getClosingCost(String line) {
    	return line.split("\\|")[7];// separator and column in which to sort.
    } 
}
