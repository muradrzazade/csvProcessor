/********************************************************
 * 	Created by Murad Rzazade on 12/03/2018
 * 	Copyright © 2018. All rights reserved.
 * 	Last modified 24/04/2018
 * 	Unauthorized copying or using of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Murad Rzazade <muradrzazade@mail.ru>, April 2018
 * 	
 * @author Murad Rzazade
 * 
 ******************************************************/

package readInReverse;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;



//This class is responsible for collection consignment article data to skip transaction that are not referred to bravos cost of sales
public class SystemConsignmentProducts {
	
	private static Map<String, List<String>> map;
	private static DateFormat movement = new SimpleDateFormat ("dd/MM/yy");
	private static DateFormat consignment = new SimpleDateFormat ("MM/dd/yyyy");
		
	public static boolean isConsignmentProduct(String articleCode) {
		
		if(map.get(articleCode) != null)
			return true;
		else
			return false;
	}
	
	public static boolean isWithinConsignmentDates(String site, String articleCode, String movementDate) throws Exception {
		
			Date trasactionDate = null, consignmentStartDate = null, consignmentEndDate=null;
			List<String> listOfLines = map.get(articleCode);

			for(String thisLine : listOfLines) {
	
    			if (site.equals(getSiteCode(thisLine))) {
    				trasactionDate = movement.parse(movementDate);
    				consignmentStartDate = consignment.parse(getStartDate(thisLine));
    				consignmentEndDate = consignment.parse(getEndDate(thisLine));
    			}
			}
			
			if(trasactionDate == null | consignmentStartDate == null | consignmentEndDate == null)
				return false;
			else if (trasactionDate.equals(consignmentStartDate)||trasactionDate.equals(consignmentEndDate)
					||trasactionDate.after(consignmentStartDate) && trasactionDate.before(consignmentEndDate))    		
				return true;
			else 
				return false;			
		}
	

	public static void getConsignmentProducts(String path) throws IOException {
	
		BufferedReader reader = new BufferedReader(new FileReader(path));
		map = new TreeMap<String, List<String>>();
    
		String line = reader.readLine();//read header (skip the header)
		while ((line = reader.readLine()) != null) {
			String key = getArticleCode(line);
			List<String> listOfLines = map.get(key);
			if (listOfLines == null) {
				listOfLines = new LinkedList<String>();
				map.put(key, listOfLines);
			}
			listOfLines.add(line);
		}	
		reader.close();		
	}

	
	private static String getArticleCode(String line) {
		return line.split("\\|")[0];
	}	
	
	private static String getSiteCode(String line) {
		return line.split("\\|")[2];
	}	
	
	private static String getStartDate(String line) {
		return line.split("\\|")[3];
	}	
	
	private static String getEndDate(String line) {
		return line.split("\\|")[4];
	}	
}
