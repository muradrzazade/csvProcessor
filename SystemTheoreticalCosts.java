/********************************************************
 * 	Created by Murad Rzazade on 12/03/2018
 * 	Copyright © 2018. All rights reserved.
 * 	Last modified 24/04/2018
 * 	Unauthorized copying or using of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Murad Rzazade <muradrzazade@mail.ru>, April 2018
 * 	
 * @author Murad Rzazade
 * 
 ******************************************************/

package readInReverse;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


//This Class is responsible for collecting theoretical costs. 
public class SystemTheoreticalCosts {

	private static Map<String, List<String>> map;
	private static DateFormat movement = new SimpleDateFormat ("dd/MM/yy");
	private static DateFormat theoretical = new SimpleDateFormat ("MM/dd/yyyy");
	
	public static double getTheoreticalCost(String site, String articleCode, String movementDate, String LVCode) throws Exception {
		
		Date trasactionDate = null, theoreticalCostStartDate = null, theoreticalCostEndDate=null;
		List<String> listOfLines = map.get(site+articleCode+LVCode);
		double theoreticalCost = 0;

		if(listOfLines!=null)			
			for(String thisLine : listOfLines) {
				trasactionDate = movement.parse(movementDate);
				theoreticalCostStartDate = theoretical.parse(getStartDate(thisLine));
				theoreticalCostEndDate = theoretical.parse(getEndDate(thisLine));
			
				if (trasactionDate.equals(theoreticalCostStartDate) || trasactionDate.equals(theoreticalCostStartDate) 
							|| trasactionDate.after(theoreticalCostStartDate) && trasactionDate.before(theoreticalCostEndDate)) {
				   					
					theoreticalCost = Double.parseDouble(getTheoreticalCost(thisLine));
				}
			}	
		
		return 	theoreticalCost;		
	}
		
	public static void getTheoreticalCosts(String path) throws IOException {
		
		BufferedReader reader = new BufferedReader(new FileReader(path));
		map = new TreeMap<String, List<String>>();
    
		String line = reader.readLine();//read header (skip the header)
		while ((line = reader.readLine()) != null) {
			
			//Theoretical costs file have more than 2 million rows
			//in order to access required cost quickly, unique key is combination of site,articlecode and SU Code.
			String key = getSiteCode(line)+getArticleCode(line)+getSUCode(line);		
			List<String> listOfLines = map.get(key);
			if (listOfLines == null) {
				listOfLines = new LinkedList<String>();
				map.put(key, listOfLines);
			}
			listOfLines.add(line);
		}	
		reader.close();		
	}
	
	private static String getSiteCode(String line) {
		return line.split("\\|")[0];
	}	
	
	private static String getArticleCode(String line) {
		return line.split("\\|")[1];
	}
	
	private static String getSUCode(String line) {
		return line.split("\\|")[2];
	}
	
	private static String getTheoreticalCost(String line) {
		return line.split("\\|")[3];
	}
	
	private static String getStartDate(String line) {
		return line.split("\\|")[4];
	}
	
	private static String getEndDate(String line) {
		return line.split("\\|")[5];
	}
	
}
