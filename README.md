# Tool for processing CSV files and calculating necessary transactions

This algorithm processes 10,000,000 rows in 200 Seconds.

As this algorithm uses heap memory, RAM memory is utilized for proccessing the files. 
Meaning that the total files size should not be more than a RAM of the device.

To allocate heap memory for JAVA environment add the following argument:
*-Xmx99G* (where 99G means 99 Gygabytes)

